---
title: "<br><br><small>**Introduction to Bayesian regression models**</small>\n"
pagetitle: "Vassilis Kehayas, `r Sys.Date()`"
author: "Vassilis Kehayas<br>https://brm-intro.netlify.app"
date: "`r Sys.Date()`"
output:
  revealjs::revealjs_presentation:
    css: custom.css
    reveal_options:
      slideNumber: true
      previewLinks: true
      hash: true
    slide_level: 4
    transition: none
---

### **Sections**

<br>
<div style="text-align:left; margin-left:250px">
- Probability theory  
- Numerical methods  
- Confounding  
- Predictive accuracy  
- Generalized linear models  
</div>

### **Probability theory (I)**

<br>
Binary logic

<br>
<small>
$$
H \rightarrow \textrm{FALSE}
\\
\not\equiv
\\
H \rightarrow \textrm{TRUE}
$$
</small>


### **Probability theory (II)**

<br>
Probability theory as an extension of binary logic

<small>
*How could we build a machine which would find the most plausible answer to a question 
based on the available information under uncertainty?*

<br>
<div style="text-align:left">
**Desiderata**

1) Degrees of plausibility are represented by real numbers
2) Qualitative correspondence with common sense
3) Consistent reasoning:
<ol class="lowerAlphaList">
<li> If a conclusion can be reasoned out in more than one way, then every possible way must lead to the same result</li>
<li>All of the relevant evidence is taken into account</li>
<li>Equivalent states of knowledge are represented by equivalent plausibility assignments</li>
</ol>
</div>

<br>
<p style="text-align:right">
Jaynes, E.T., 2003.  
Probability Theory: The Logic of Science.  
Cambridge University Press.
</p>
</small>

### **Probability theory (III)**

<br>
The product and sum rules

<div style="font-size:0.7em; line-height:2.5em">
1) Product rule
$$\\ p(AB|C) = p(A|BC)p(B|C) = p(B|AC)p(A|C)$$
2) Sum rule
$$\\ p(A|B) + p(\overline{A}|B) = 1$$
</div>

### **Probability theory (IV)**

<br>
Sampling earth for water --- example

<br>
<small>
Data:  
\[D =
\begin{bmatrix}
W & L & W & W & W & L & W & L & W
\end{bmatrix}
\]  
Posterior distribution:  
$$Pr(p_W|D) = \frac{Pr(D|p_W)Pr(p_W)}{Pr(D)}$$  
Likelihood:  
$$Pr(D|p_W) = \frac{(N_W+N_L)!}{N_W!N_L!}p_W^{N_W}(1-p_W)^{N_L}$$  
Marginal likelihood:  
$$Pr(D) = E \big (Pr(D|p_W) \big) = \int{Pr(D|p_W)Pr(p_W)dp_W}$$
</small>

### **Probability theory (V)**

<br>
Sampling earth for water --- model

<small>
<br>
$$W \sim \text{Binomial}(N, p)\\ 
p \sim \text{Uniform}(0,1)$$
</small>

```{r gridApproximationExample, echo=FALSE, message=FALSE, out.width=550}
ApproximateGridStep = function(nSuccess,
                               nTrials,
                               prior,
                               from,
                               to,
                               length.out) {
  
  # Probability grid
  p_grid = seq(from,
               to,
               length.out = length.out)
  
  # Compute likelihood
  likelihood = dbinom(nSuccess,
                      size = nTrials,
                      prob = p_grid)
  
  # Un-standardized posterior
  unstdPosterior = likelihood * prior
  
  # Posterior
  posterior = unstdPosterior / sum(unstdPosterior)
  
  return(posterior)
  
}

CreateBaseTheme = function(...) {
  
  light_theme =
    theme_classic() +
    theme(text = element_text(size = 18,
                              face = "bold"),
          plot.subtitle = element_text(face = "plain"),
          plot.caption = element_text(face = "plain",
                                      size = 12),
          ...)
  
  return(light_theme)
  
}

fg = function(x) {formatC(x, format = "fg")}

library(data.table)
library(ggplot2)
library(gganimate)

dataVector = c("W", "L", "W", "W", "W", "L", "W", "L", "W")
nTrials = seq_along(dataVector)
isW = as.integer(dataVector == "W")

gridDimension = 50
prior = rep(1, gridDimension)
posteriorList = list()

for (ii in 1:length(isW)) {
  
  posterior = ApproximateGridStep(isW[ii],
                                  1,
                                  prior,
                                  0,
                                  1,
                                  gridDimension)
  posteriorList[[ii]] = list(nW = isW[ii],
                             trial = nTrials[ii],
                             prior = prior,
                             posterior = posterior,
                             dataValue = dataVector[ii],
                             p = c(1:gridDimension)/gridDimension)
  prior = posterior
}

posteriorTable = data.table::rbindlist(posteriorList)

posteriorTable = melt(posteriorTable,
                      id.vars = c("nW",
                                  "trial",
                                  "dataValue",
                                  "p"),
                      variable.name = "distClass",
                      value.name = "distValue")
posteriorTable[, distClass := factor(distClass, 
                                     levels = c("prior",
                                                "posterior"))]
posteriorTable[, 
               normDistValue := distValue/sum(distValue), 
               by = .(trial,
                      distClass)]
posteriorTable[, 
               strip := paste0("Trial ", 
                               trial, 
                               ", ", 
                               dataValue)]
gp = 
  ggplot(data = posteriorTable,
         aes(x = p,
             y = normDistValue,
             linetype = distClass)) +
  geom_line(size = 1) +
  facet_wrap(~strip) +
  # gganimate::transition_states(strip) +
  scale_linetype_manual(values = c("dashed",
                                   "solid")) +
  labs(x = "p\n(proportion water)",
       y = "Probability",
       linetype = NULL) +
  # ggtitle("{closest_state}") +
  scale_x_continuous(labels = fg) +
  scale_y_continuous(labels = fg) +
  CreateBaseTheme(legend.key.width = unit(3, "char"))

gp

# gganimate::animate(gp,
#                    nframes = 72,
#                    duration = 18,
#                    renderer = ffmpeg_renderer(),
#                    rewind = TRUE)
# gganimate::anim_save("img/globe-tossing.mp4")

```
<!-- <video data-autoplay loop="true" src="img/globe-tossing.mp4" style="width:50%"></video> -->

### **Probability theory (VI)**

<br>
The influence of priors

```{r priors, echo=FALSE, out.width=600}
library(patchwork)
library(grid)

gridDimension = 300
p_grid = seq(0,
             1,
             length.out = gridDimension)

m = 0.5
s = 0.15

flatPrior = rep(1, length.out = gridDimension)
normalPrior = dnorm(p_grid, m, s)
laplacePrior = exp(-abs(p_grid - m) / s) / (2 * s)
priorList = list(Flat = flatPrior,
                 Normal = normalPrior,
                 Laplace = laplacePrior)

likelihood = dbinom(sum(dataVector == "W"),
                    length(dataVector),
                    prob = p_grid)

posteriorMatrix = sapply(priorList, 
                         function(prior, 
                                  likelihood) {
                           unstandardizedPosterior = prior*likelihood
                           posterior = unstandardizedPosterior/sum(unstandardizedPosterior)
                         },
                         likelihood)

priorTable = cbind(as.data.table(priorList),
                   p_grid)
priorTable = melt(priorTable, 
                  id.vars = "p_grid", 
                  variable.name = "prior", 
                  value.name = "distValue")
posteriorTable = data.table(as.data.table(posteriorMatrix),
                            p_grid)
posteriorTable = melt(posteriorTable, 
                      id.vars = "p_grid", 
                      variable.name = "posterior", 
                      value.name = "distValue")

priorPlot = 
  ggplot(data = priorTable,
         aes(x = p_grid,
             y = distValue)) +
  geom_line(size = 1) +
  facet_wrap(~prior, 
             ncol = 1, 
             strip.position = "left") +
  labs(title = "Priors") +
  theme_void() +
  theme(text = element_text(size = 18,
                            face = "bold"),
        plot.title = element_text(hjust = 0.5),
        strip.text = element_text(size = 20))

spacerPlotWithTitle = 
  ggplot() +
  labs(title = "Likelihood") +
  theme_void() +
  theme(text = element_text(size = 18,
                            face = "bold"),
        plot.title = element_text(hjust = 0.5))

likelihoodPlot = 
  ggplot() +
  geom_line(mapping = aes(x = p_grid,
                          y = likelihood),
            size = 1) +
  theme_void()

posteriorPlot = 
  ggplot(data = posteriorTable,
         aes(x = p_grid,
             y = distValue)) +
  geom_line(size = 1) +
  facet_wrap(~posterior, 
             ncol = 1) +
  labs(title = "Posteriors") +
  theme_void() +
  theme(strip.background = element_blank(),
        strip.text.x = element_blank(),
        text = element_text(size = 18,
                            face = "bold"),
        plot.title = element_text(hjust = 0.5))

propPlot = 
  ggplot() +
  geom_text(mapping = aes(x = 2.5, 
                          y = 5, 
                          label = ". %prop% ."),
            parse = TRUE, 
            fontface = "bold",
            size = 24) +
  theme_void()

priorPlot + 
  grid::textGrob('x',
                 gp = grid::gpar(fontsize = 42,
                                 fontface = "bold")) + 
  (spacerPlotWithTitle / likelihoodPlot / plot_spacer()) + 
  propPlot +
  posteriorPlot +
  plot_layout(ncol = 5, widths = c(1, 0.1, 1, 0.24, 1))

```

### **Numerical methods (I)**

<br>
Grid approximation

<div style="font-size:0.81em">
```{r, eval=FALSE}
gridDimension = 50

# Define probability grid
p_grid = seq(from = 0,
             to = 1,
             length.out = gridDimension)

# Define prior
prior = rep(1, gridDimension)

# Compute likelihood
likelihood = dbinom(nSuccess,
                    size = nTrials,
                    prob = p_grid)

# Un-standardized posterior
unstdPosterior = likelihood * prior

# Posterior
posterior = unstdPosterior / sum(unstdPosterior)
```
</div>
<div style="text-align:left;font-size:0.81em">
PROs: Intuitive  
CONs: Does not scale well
</div>

### **Numerical methods (II)**

<br>
Quadratic approximation

<br>
<div style="text-align:left;font-size:0.81em">
- With flat priors, equivalent to maximum likelihood estimation  
- With non-flat priors, equivalent to penalized maximum likelihood estimation  

<br>
PROs: Computationally less costly  
CONs: Limited, approximate
</div>

### **Numerical methods (III)**

<br>
Variational inference

<br>
<small>
$$ \boldsymbol{\phi^*} = \underset{\boldsymbol{\phi} \in \boldsymbol{\Phi}}{\operatorname{arg min}} \text{KL} \big ( q( \boldsymbol \theta ; \boldsymbol \phi) || p(\boldsymbol \theta | \boldsymbol x)\big ),$$
for a family of approximating densities $q( \boldsymbol \theta ; \boldsymbol \phi)$, parametrized by a vector $\boldsymbol{\phi} \in \boldsymbol{\Phi}$, and a posterior $p(\boldsymbol \theta | \boldsymbol x)$.
</small>

<br>
<div style="text-align:left;font-size:0.81em">
PROs: Computationally less costly  
CONs: Approximate
</div>

<br>
<small>
<div style="text-align:right; width:800px">
Kucukelbir et al., 2016.  
arXiv:1603.00788.
</div>
</small>

### **Numerical methods (IV)**

<br>
Markov-chain Monte-Carlo:  
Metropolis algorithm

<br>
<small>
<div style="text-align:left">
**Steps:**  
1. Set initial position $x_t$.  
2. Set proposal position $x^\prime$.  
3. Set $x_{t+1} = x^\prime$ with probability $\propto \frac{Pr(x^\prime)}{Pr(x_t)}$.  
</div>
</small>

### **Numerical methods (V)**

Markov-chain Monte-Carlo:  
Metropolis algorithm -- $2d$ Gaussian

<br>
<video data-autoplay loop="true" src="img/rw_2dGaussian.mp4">
</video>

<small>
https://github.com/chi-feng/mcmc-demo
</small>

### **Numerical methods (VI)**

Concentration of measure in high-dimensional Gaussians

<br>
<video data-autoplay loop="true" src="img/high-d-gaussian.mp4" style="height:450px">
</video>

<small>
https://github.com/ae14watanabe/high-dimensional_gaussian_is_like_sphere
</small>

#### {-}
<h3><strong>Numerical methods (VI)</strong></h3>

<br>
Another geometrical interpretation of the concentration of measure phenomenon

<br>
<img src="img/concentration_of_measure.png"></img>

<br>
<small>
<div style="text-align:right; width:900px">
Adapted from
Betancourt, 2019. Probabilistic Computation.  
Retrieved from <a href="https://github.com/betanalpha/knitr_case_studies/tree/b474ec1a5a79347f7c9634376c866fe3294d657a">https://github.com/betanalpha/knitr_case_studies</a>
</div>
</small>

### **Numerical methods (VII)**

Markov-chain Monte-Carlo:  
Metropolis algorithm -- Donut

<video data-autoplay loop="true" src="img/rw_donut.mp4">
</video>

<small>
https://github.com/chi-feng/mcmc-demo
</small>

### **Numerical methods (VIII)**

Markov-chain Monte-Carlo:  
Hamiltonian Monte-Carlo

<small>
*Move a frictionless particle to positions on a frictionless surface
according to a Hamiltonian function*  
$$\begin{aligned}
H(\rho, \theta) & = T(\rho|\theta) + V(\theta),\\
T(\rho|\theta)  & = -\log \big( Pr(\rho|\theta) \big),\\
V(\theta)    & = - \log \big ( Pr(\theta) \big ),
\end{aligned}$$
where $\rho$ the introduced momentum of the particle, $Pr(\theta)$ the density of parameters $\theta$.

<br>
<div style="text-align:left">
<strong>Steps (simplified):</strong>  
1. Set initial position $\theta_t$, initial momentum $\rho_t \sim \text{Multinormal(0, M)}$  
2. Calculate proposal state $(\rho^*, \theta^*)$ after $L$ steps of size $\epsilon$ based on the leapfrog integrator  
3. Accept proposal state $(\rho^*, \theta^*)$ with probability $\exp \big (H(\rho_t, \theta_t) −H(\rho^*, \theta^*) \big )$
</div>
<br>
</small>
<div style="text-align:right; font-size:0.6em">
Neal R., 2011. MCMC Using Hamiltonian Dynamics,   
in: Brooks, S., Gelman, A., Jones, G., Meng, X.-L. (Eds.),  
Handbook of Markov Chain Monte Carlo,  
Chapman and Hall/CRC.  
https://doi.org/10.1201/b10905-6
</div>

### **Numerical methods (IX)**

Markov-chain Monte-Carlo:  
Hamiltonian Monte-Carlo -- Donut

<video data-autoplay loop="true" src="img/hmc_donut.mp4">
</video>

<small>
https://github.com/chi-feng/mcmc-demo
</small>

### **Numerical methods (X)**

Markov-chain Monte-Carlo:  
Hamiltonian Monte-Carlo -- $U$-turns

<video data-autoplay loop="true" src="img/hmc_uturns.mp4">
</video>
<small>
https://github.com/chi-feng/mcmc-demo
</small>

### **Numerical methods (XI)**

Markov-chain Monte-Carlo:  
No $U$-turn sampler  
<small>
(NUTS, Hoffman & Gelman, 2011. arXiv:1111.4246)
</small>
<br>
<video data-autoplay loop="true" src="img/nuts_2d_gaussian.mp4" style="height:50%">
</video>
<small>
https://github.com/chi-feng/mcmc-demo
</small>

### **Numerical methods (XII)**

Markov-chain Monte-Carlo:  
Parameter samples

<div style="font-size:0.4em">
$$\begin{aligned}
y_i & = \text{Normal}(\alpha + \beta x_i, \sigma)\\
\alpha & \sim \text{Normal}(0, 1)\\
\beta & \sim \text{Normal}(0, 1)\\
\sigma & \sim \text{Exponential}(1)\\
\end{aligned}$$
</div>

```{r caterpillar, echo=FALSE, message=FALSE, results='hide', warning=FALSE, fig.height=4.5}
library(brms)

set.seed(0)
n = 100
x = rnorm(n)
beta = 0.5
alpha = 1
y = alpha + beta*x + rnorm(n, 0, 0.25)

d = data.table(x, y)

samplingModel = brm(y ~ 0 + Intercept + x,
                    data = d,
                    family = gaussian(),
                    prior = c(prior(normal(0, 1), coef = "Intercept"),
                              prior(normal(0, 1), class = "b"),
                              prior(exponential(1), class = "sigma")),
                    chains = 4,
                    cores = 4,
                    iter = 2000,
                    file = "mdl/samplingExample",
                    file_refit = "on_change")

plot(samplingModel, 
     combo = c("dens_overlay", 
               "trace"), 
     binwidth = 0.01, 
     theme = CreateBaseTheme())
```

### **Confounding (I)**

<br>
Possibilities of confounding  
<small>
(J. Pearl's causal inference framework)
</small>

```{r dagFramework, echo=FALSE, fig.height=3.25, out.width=800}
library(dagitty)
library(ggdag)
library(png)

PlotDAG = function(dagObj) {
  
  ggplot(tidy_dagitty(dagObj),
         aes(x = x, 
             y = y, 
             xend = xend, 
             yend = yend)) +
    geom_dag_text(mapping = aes(label = name),
                    colour = "black",
                    size = 8) +
    lims(x = c(0.5, 3.5),
         y = c(0.75, 2.75)) +
    geom_dag_edges_link(edge_width = 1.5,
                        arrow = arrow(length = unit(1, "char"))) +
    theme_void()
  
}

forkDAG = dagitty("dag{
               X <- Z -> Y
}")
coordinates(forkDAG) = list(x = c(Z = 2, X = 1, Y = 3),
                            y = c(Z = 2, X = 1, Y = 1))
forkDAGPlot = 
  PlotDAG(forkDAG) +
  annotate("text",
           x = 2,
           y = 2.5,
           label = "The Fork",
           fontface = "bold",
           size = 8)

pipeDAG = dagitty("dag{
               X -> Z -> Y
}")

coordinates(pipeDAG) = list(x = c(Z = 2, X = 1, Y = 3),
                            y = c(Z = 2, X = 1, Y = 1))
pipeDAGPlot = 
  PlotDAG(pipeDAG) +
  annotate("text",
           x = 2,
           y = 2.5,
           label = "The Pipe",
           fontface = "bold",
           size = 8)

colliderDAG = dagitty("dag{
               X -> Z <- Y
}")

coordinates(colliderDAG) = list(x = c(Z = 2, X = 1, Y = 3),
                                y = c(Z = 2, X = 1, Y = 1))
colliderDAGPlot = 
  PlotDAG(colliderDAG) +
  annotate("text",
           x = 2,
           y = 2.5,
           label = "The Collider",
           fontface = "bold",
           size = 8)

dagConfoundsPlot = 
  forkDAGPlot +
  pipeDAGPlot +
  colliderDAGPlot

dagConfoundsPlot
```


### **Confounding (II)**

<br>
The fork  
<small>
(Omitted variable bias)
</small>
```{r forkDAG, echo=FALSE, out.width=800, fig.height=3}

heatDAG = dagitty("dag{
               \"Ice cream\" <- Heat -> Crime
}")
coordinates(heatDAG) = list(x = c(Heat = 2, 'Ice cream' = 1, Crime = 3),
                            y = c(Heat = 2.5, 'Ice cream' = 1, Crime = 1))

heatDAGPlot = PlotDAG(heatDAG)

iceCreamCrimeCorrelationImage = png::readPNG("img/iceCream_vs_crime.png",
                                             native = TRUE)

heatDAGPlot + 
  inset_element(forkDAGPlot +
                  theme(panel.border = element_rect(colour = "black", 
                                                    fill = NA, 
                                                    size = 2)), 
                left = 0, top = 1, bottom = 0.5, right = 0.3,
                align_to = "full",
                clip = FALSE) +
  inset_element(iceCreamCrimeCorrelationImage,
                left = 0.675, top = 1, bottom = 0.5, right = 1,
                align_to = "full",
                clip = FALSE)
```

$$ X \perp \!\!\! \perp Y | Z $$
Solution: Control for $Z$

### **Confounding (III)**

<br>
The pipe  
<small>
(Post-treatment bias)
</small>
```{r pipeDAG, echo=FALSE, out.width=800, fig.height=3}
cortisolDAG = dagitty("dag{
               Drug -> Cortisol -> Memory
}")

coordinates(cortisolDAG) = list(x = c(Cortisol = 2, Drug = 1, Memory = 3),
                                y = c(Cortisol = 2.5, Drug = 1, Memory = 1))

cortisolDAGPlot = PlotDAG(cortisolDAG)
cortisolDAGPlot + 
  inset_element(pipeDAGPlot +
                  theme(panel.border = element_rect(colour = "black", 
                                                    fill = NA, 
                                                    size = 2)), 
                left = 0, top = 1, bottom = 0.5, right = 0.3,
                align_to = "full",
                clip = FALSE)
```

$$ X \perp \!\!\! \perp Y | Z $$
Solution: Do not control for $Z$

### **Confounding (IV)**

<br>
The collider  
<small>
(Selection bias)
</small>  
```{r colliderDAG, echo=FALSE, out.width=800, fig.height=3}
lightDAG = dagitty("dag{
               Switch -> Light <- Electricity
}")

coordinates(lightDAG) = list(x = c(Light = 2, Switch = 1, Electricity = 3),
                             y = c(Light = 2.5, Switch = 1, Electricity = 1))

lightDAGPlot = PlotDAG(lightDAG)
lightDAGPlot + 
  inset_element(colliderDAGPlot +
                  theme(panel.border = element_rect(colour = "black", 
                                                    fill = NA, 
                                                    size = 2)), 
                left = 0, top = 1, bottom = 0.5, right = 0.3,
                align_to = "full",
                clip = FALSE)
```

$$ X \perp \!\!\! \perp Y $$
Solution: Do not control for $Z$

### **Predictive accuracy (I)**

<small>
$$ \begin{aligned}
f1: \ & b_i \sim \ \text{Normal}(\alpha + \beta_1 m_i, \sigma)\\
f2: \ & b_i \sim \ \text{Normal}(\alpha + \beta_1 m_i + \beta_2 m_i^2, \sigma)\\
\ldots\\
f6: \ & b_i \sim \ \text{Normal}(\alpha + \beta_1 m_i + \beta_2 m_i^2 +  \beta_3 m_i^3 + \beta_4 m_i^4 + \beta_5 m_i^5 + \beta_6 m_i^6, \sigma)
\end{aligned}$$
</small>

```{r overfit, echo=FALSE, message=FALSE, warning=FALSE, fig.height=5}
library(data.table)
library(ggplot2)

d = data.table(
  species = c("afarensis", "africanus", "habilis", "boisei", "rudolfensis", "ergaster", "sapiens"),
  brain = c(438, 452, 612, 521, 752, 871, 1350), # brain volume in CC
  mass = c(37.0 , 35.5, 34.5, 41.5, 55.5, 61.0, 53.5)) # body mass in kg

PlotPolynomialFit = function(p, data) {
  
  gp = 
    ggplot(data = data,
           aes(x = mass,
               y = brain)) +
    geom_point() +
    geom_smooth(method = "lm",
                formula = paste0("y ~ poly(x, ", p, ")")) +
    coord_cartesian(ylim = c(-300, 1800)) +
    ggtitle(paste0("f", p)) +
    CreateBaseTheme() +
    labs(x = "Body mass\n(kg)",
         y = "Brain volume\n(cc)")
  
  if (p %in% c(2, 3, 5, 6)) {
    gp = gp +
      labs(y = NULL)
  }
  
  if (p %in% c(1, 2, 3)) {
    gp = gp +
      labs(x = NULL)
  }
  
  if (p == 6) {
    gp = gp +
      geom_hline(yintercept = 0,
                 linetype = "dashed",
                 colour = "grey50",
                 size = 1)
  }
  
  return(gp)
  
}

gpList = lapply(1:6, PlotPolynomialFit, d)
patchwork::wrap_plots(gpList)
```

### **Predictive accuracy (II)**

<br>
Information theory:  
Distribution entropy  

<small>
$$ H(p) = −E\log(p_i) = −\sum_{i=1}^n p_i\log(p_i) $$  
*The expected uncertainty contained in a probability distribution is  
the (negative) expected log-probability of an event.*
</small>

### **Predictive accuracy (III)**

<br>
Information theory:  
Divergence  

<small>
$$ \begin{aligned}
D_{KL}(p, q) & = H(p, q) - H(p)\\
&= -\sum_i p_i\log(q_i) - \big (-\sum_i p_i\log(p_i) \big) \\ 
& = \sum_i p_i \big (\log(p_i) - \log(q_i) \big) \\ 
& = \sum_ip_i\log \Bigg(\frac{p_i}{q_i} \Bigg)
\end{aligned}$$  
*Divergence is the expected difference in log probability  
between the target (p) and the model (q).*
</small>

### **Predictive accuracy (IV)**

<br>
Information theory:  
Log-pointwise predictive density  

<small>
Log-score  
$$ S(q) = \sum_i\log(q_i)$$  
Log-pointwise predictive density  
$$\text{lppd} = \sum_{i=1}^N\log\frac{1}{S}\sum_{s=1}^S p(y_i|\Theta_s)$$  
Deviance  
$$ \text{deviance} = -2 \cdot \text{lppd} $$
</small>


### **Predictive accuracy (V)**

<br>
Information theory:  
Pareto-smoothed cross-validation  

<small>
Leave-one-out (LOO) cross-validated lppd:  
$$ \text{lppd}_{\text{LOO}} = \sum_{i=1}^N \frac{1}{S}\sum_{s=1}^S \log \big(p(y_i|\theta_{-i,s}) \big ) $$  
Pareto-smoothed importance-sampling cross-validation  
$$ \text{lppd}_\text{PSIS-LOO} = \sum_{i = 1}^N \log \Bigg ( \frac{\sum_{s=1}^S w_i^s p(y_i|\theta_s)}{\sum_{s=1}^S w_i^s} \Bigg )$$  
<div style="text-align:right; width:700px; bottom: -25%; position: fixed;">
Vehtari, Gelman, & Gabry, 2017.  
Stat Comput 27, 1413–1432.  
https://doi.org/10.1007/s11222-016-9696-4
</div>
</small>

### **Predictive accuracy (VI)**

<br>
Information theory:  
Widely-applicable (Watanabe) information criterion  

<small>
$$ \text{lppd}_\text{WAIC} = -2(\text{lppd} - \hat{p}_\text{WAIC}) ,$$  
where
$$ \hat{p}_\text{WAIC} = \sum_{i = 1}^N \mathrm{Var}_\theta [\log \big (p(y_i|\theta) \big )] $$  
<div style="text-align:right; width:700px; bottom: -25%; position: fixed;">
Watanabe 2010. Neural Networks 23, 20–34.  
https://doi.org/10.1016/j.neunet.2009.08.002
</div>
</small>

### **Predictive accuracy (VII)**

Information theory:  
Metric comparison  

<img src="img/rethinking_CV1.png" style="width:125%">

<small>
<div style="text-align:right; bottom: 0%; position: fixed;">
Adapted from McElreath 2020. *Statistical rethinking: A Bayesian course with examples in R and Stan*.  
CRC press.
</div>
</small>

### **Predictive accuracy (VII)**

Information theory:  
Metric comparison  

<img src="img/rethinking_CV2.png" style="width:125%">

<small>
<div style="text-align:right; bottom: 0%; position: fixed;">
Adapted from McElreath 2020. *Statistical rethinking: A Bayesian course with examples in R and Stan*.  
CRC press.
</div>
</small>

### **Generalized linear models (I)**

<img src="img/statistical_tests.jpg">

<small>
Common statistical tests as linear models:  
https://lindeloev.github.io/tests-as-linear
</small>

### **Generalized linear models (II)**

<br>
Simple linear model:  

<small>
<br>
$$ \begin{aligned} 
\boldsymbol{y} & \sim \text{Normal}(\boldsymbol{\mu}, \boldsymbol{\sigma})\\
\boldsymbol{\mu}  & = \boldsymbol{B} \boldsymbol{X}
\end{aligned}$$

Maximum entropy distribution given $\boldsymbol{\mu} \in \mathbb{R}, \boldsymbol{\sigma}^2 > 0$
</small>

### **Generalized linear models (III)**

<br>
Linear regression is a bad fit for binary outcomes:  

```{r linearFitBinaryData, echo=FALSE, message=FALSE, fig.height=4, fig.width=5}
library(brms)
library(ggplot2)

x = rnorm(100)
yLin = 1.5*x + rnorm(100, 0, 0.5)
p = as.integer(inv_logit_scaled(yLin) > 0.5)

linPred = predict(lm(p ~ x))
logPred = predict(glm(p ~ x,
                      family = binomial()),
                  type = "response")

linFitPlot = 
  ggplot(mapping = aes(x = x)) +
  geom_point(mapping = aes(y = p),
             size = 4,
             alpha = 0.33,
             stroke = FALSE,
             shape = 21,
             fill = "black") +
  geom_line(mapping = aes(y = linPred),
            size = 2,
            colour = "#0571b0",
            alpha = 0.87) +
  CreateBaseTheme() +
  scale_y_continuous(labels = fg)

linFitPlot
```

### **Generalized linear models (IV)**

<br>
Logistic regression:  
<small>
<br>
$$\begin{aligned} 
\boldsymbol{y} & \sim \text{Binomial}(n, \boldsymbol{p})\\
\text{logit}(\boldsymbol{p}) & = \boldsymbol{B} \boldsymbol{X}
\end{aligned},$$

where $\text{logit}(p) = \log \frac{p}{1 - p}$

Maximum entropy distribution given $E(\boldsymbol{y}) = n\boldsymbol{p}$
</small>

```{r logistic, echo=FALSE, fig.height=3, fig.width=3.75}
linFitPlot +
  geom_line(mapping = aes(y = logPred),
            colour= "#ca0020",
            size = 2,
            alpha = 0.87)

```

### **Generalized linear models (V)**

<br>
The exponential family of distributions  

<img src="img/exponential_family.png" style="height:400px">

<small>
<div style="text-align:right; bottom: 0%; position: fixed;">
McElreath 2020. *Statistical rethinking: A Bayesian course with examples in R and Stan*.  
CRC press.
</div>
</small>

### **Generalized linear models (VI)**

Poisson example

<small>
Fixed intercept:
<br>
<br>
$$\begin{aligned}
y_i & \sim \text{Poisson}(\lambda_i)\\
\log(\lambda_i) & =  \alpha\\
\alpha & \sim \text{Normal}(t, 1)\\
t & = \log (18 \ \text{spikes}) \approx 2.9\\
\phantom{1}\\
\phantom{1}
\end{aligned}$$
</small>

```{r poissonExample1, echo=FALSE, message=FALSE, fig.height=3.5, fig.width=4}
library(data.table)
library(brms)

set.seed(0)

nNeurons = 20
nReplicates = 1
durationSec = 30*60 # Half hour in sec
rateHz = 0.01 # Expected firing rate in Hz
logSpikeNumber = log(rateHz*durationSec)

logMu = logSpikeNumber + rnorm(nNeurons, 0, 0.25)
lFun = function(logMu, nReplicates) {rpois(nReplicates, exp(logMu))}
y = unlist(mapply(lFun, 
                  logMu = logMu, 
                  nReplicates = nReplicates, 
                  SIMPLIFY = FALSE, 
                  USE.NAMES = TRUE))

poissonSimData = data.table(iNeuron = rep(1:nNeurons, 
                                          times = nReplicates),
                            iReplicate = rep(nReplicates,
                                             times = nReplicates),
                            y)
poissonSimData[, meanY := mean(y), by = iNeuron]
setorder(poissonSimData, iReplicate, meanY, iNeuron)
poissonSimData[, sI := factor(rleid(iNeuron))]

glm1 = brm(y ~ 0 + Intercept,
           family = poisson(),
           data = poissonSimData,
           prior = prior(normal(2.9, 1), coef = "Intercept"),
           chains = 2,
           cores = 2,
           iter = 4000,
           file = "mdl/glm1",
           file_refit = "on_change",
           refresh = 0)

pred_glm1 = exp(fixef(glm1)[, 1])

t = exp(logSpikeNumber)

gp1 = 
  ggplot(data = poissonSimData,
         aes(x = as.numeric(sI))) +
  geom_line(mapping = aes(y = pred_glm1 - t,
                          colour = "Fixed"),
            size = 2,
            alpha = 0.8) +
  geom_point(mapping = aes(y = y - t),
             size = 2,
             alpha = 0.8) +
  CreateBaseTheme() +
  labs(x = "Neuron ID",
       y = bquote(bold(hat(y[i]) - t))) +
  scale_colour_manual(name = NULL,
                      values = c("#120084",
                                 "#A91892",
                                 "#F89447")) +
  theme(legend.position = c(0.25, 0.75))

gp1
```

### **Generalized linear models (VII)**

Poisson example

<small>
Individual intercepts:
<br>
<br>
$$\begin{aligned}
y_i & \sim \text{Poisson}(\lambda_i)\\
\log(\lambda_i) & = u_{\text{neuron}}\\
u_{\text{neuron}} & \sim \text{Normal}(t, 1)\\
t & = \log (18 \ \text{spikes}) \approx 2.9\\
\phantom{1}\\
\phantom{1}\\
\end{aligned}$$
</small>

```{r poissonExample2, echo=FALSE, message=FALSE, fig.height=3.5, fig.width=4}
library(data.table)
library(brms)

set.seed(0)

glm2 = update(glm1,
              formula = y ~ 0 + sI,
              family = poisson(),
              prior = prior(normal(2.9, 1), class = "b"),
              newdata = poissonSimData,
              file = "mdl/glm2",
              file_refit = "on_change",
              refresh = 0)

pred_glm2 = exp(fixef(glm2)[, 1])

gp2 = 
  gp1 +
  geom_point(mapping = aes(y = pred_glm2 - t,
                           colour = "Individual"),
             size = 3,
             alpha = 0.8)

gp2
```

### **Generalized linear models (VIII)**

Poisson example

<small>
"Varying" intercepts:
<br>
<br>
$$\begin{aligned}
y_i & \sim \text{Poisson}(\lambda_i)\\
\log(\lambda_i) & =  \alpha + u_{\text{neuron}}\\
\alpha & \sim \text{Normal}(t, 1)\\
u_{\text{neuron}} & \sim \text{Normal}(0,\sigma)\\
\sigma & \sim \text{Exponential}(t)\\
t & = \log (18 \ \text{spikes}) \approx 2.9
\end{aligned}$$
</small>

```{r poissonExample3, echo=FALSE, message=FALSE, results='hide', warning=FALSE, fig.height=3.5, fig.width=4}
library(data.table)
library(brms)

set.seed(0)

glme1 = update(glm2,
               formula = y ~ 0 + Intercept + (1|sI),
               family = poisson(),
               prior = c(prior(normal(2.9, 1), coef = "Intercept"),
                         prior(exponential(2.9), class = "sd")),
               newdata = poissonSimData,
               chains = 2,
               cores = 2,
               iter = 4000,
               file = "mdl/glme1",
               file_refit = "on_change",
               refresh = 0)

pred_glme1 = exp(coef(glme1)$sI[, , 1][, 1])

gp3 = 
  gp2 +
  geom_line(mapping = aes(y = exp(fixef(glme1)[, 1]) - t,
                          colour = "Varying"),
            size = 2,
            alpha = 0.8) +
  geom_point(mapping = aes(y = pred_glme1 - t,
                           colour = "Varying"),
             size = 3,
             alpha = 0.8)

gp3
```

### **Generalized linear models (IX)**

Poisson example

<small>
Model comparison
</small>

```{r waicPoisson, echo=FALSE, message=FALSE, warning=FALSE}

GetWAICEstimate = function(mdl, estimateName) {
  
  waicObj = waic(mdl)
  estimateValue = waicObj$estimates[rownames(waicObj$estimates) == estimateName, 
                                    colnames(waicObj$estimates) == "Estimate"]
  
  return(estimateValue)
  
}

mdlList = list(glm1, glm2, glme1)
Model = c("Fixed intercept", "Individual intercept", "Varying intercept")
WAIC = sapply(mdlList, GetWAICEstimate, "waic")
pWAIC = sapply(mdlList, GetWAICEstimate, "p_waic")

knitr::kable(data.table(Model, WAIC, pWAIC), digits = 2)

```

### **Generalized linear models (X)**

Poisson example

<div style="font-size:0.4em">
Fixed time:
<br><br>
$$\begin{aligned}
y_i & \sim \text{Poisson}(\lambda_i)\\
\log(\lambda_i) & =  \alpha + u_{\text{neuron}} + \beta x_i, \ x \in (0, 1, 2)\\
\alpha & \sim \text{Normal}(2.9, 1)\\
u_{\text{neuron}} & \sim \text{Normal}(0,\sigma)\\
\sigma & \sim \text{Exponential}(1)\\
\beta & \sim \text{Normal}(0, 1)\\
\end{aligned}$$
</div>

```{r poissonExampleFixedTime, echo=FALSE, message=FALSE, results='hide', warning=FALSE, fig.height=3.5, fig.width=4}
library(MASS)
library(data.table)
library(brms)
library(ggplot2)

set.seed(0)

nNeurons = 20
nSessions = 3
durationSec = 30*60 # Half hour in sec
rateHz = 0.01 # Expected firing rate in Hz
betaTime = 0.25
nSpikes = rateHz*durationSec
logSpikeNumber = log(nSpikes)

a = logSpikeNumber
b = betaTime
s_a = 0.1
s_b = 0.15
r = -0.7
mu = c(a, b)
cov_ab = s_a * s_b * r
S = matrix(c(s_a^2, cov_ab,
             cov_ab, s_b^2),
           ncol = 2)

varyingEffects = mvrnorm(nNeurons, mu, S)
a_neuron = varyingEffects[, 1]
b_neuron = varyingEffects[, 2]
iN = rep(1:nNeurons, each = nSessions)
iS = as.numeric(rep(0:(nSessions - 1), nNeurons))
logLambda = a_neuron + b_neuron*iS
s = 0.1
y = round(exp(rnorm(nNeurons*nSessions, logLambda, s)))

timeData = data.table(iN, iS, y)

brmFixedTime = brm(y ~ 0 + Intercept + iS + (1|iN),
                   data = timeData,
                   family = poisson(link = "log"),
                   prior = c(prior(normal(2.9, 1), class = "b", coef = "Intercept"),
                             prior(exponential(1), class = "sd"),
                             prior(normal(0, 1), class = "b")),
                   cores = 2,
                   chains = 2,
                   iter = 4000,
                   file = "mdl/fixedTime", 
                   file_refit = "on_change")

fittedFixedTime = fitted(brmFixedTime)[, 1]
yLabels = pretty(range(timeData[, y]))
yBreaks = log(yLabels)

gpFixedTime = 
  ggplot(data = timeData,
         aes(x = iS,
             y = log(y),
             group = iN)) +
  # geom_point(size = 5,
  #            alpha = 0.25,
  #            shape = 21,
  #            stroke = FALSE,
  #            fill = "black") +
  geom_line(mapping = aes(y = log(fittedFixedTime),
                          colour = "Fixed time"),
            size = 1,
            alpha = 0.3) +
  CreateBaseTheme() +
  labs(x = bquote(bold(x[i])),
       y = bquote(bold(hat(y[i])))) +
  scale_x_continuous(labels = fg,
                     breaks = c(0, 1, 2)) +
  scale_y_continuous(breaks = yBreaks,
                     labels = yLabels) +
  scale_colour_viridis_d(name = NULL, 
                         option = "C", 
                         end = 0.75) +
  theme(legend.position = "none")

gpFixedTime
```

### **Generalized linear models (XI)**

Poisson example

<div style="font-size:0.4em">
Varying time:
<br><br>
$$\begin{aligned}
y_i & \sim \text{Poisson}(\lambda_i)\\
\log(\lambda_i) & =  \alpha + u_{\text{neuron}} + \gamma_{\text{neuron}} x_i, \ x \in (0, 1, 2)\\
\alpha & \sim \text{Normal}(2.9, 1)\\
\begin{bmatrix} u_{\text{neuron}} \\ \gamma_{\text{neuron}} \end{bmatrix} & \sim \operatorname{MVNormal} \begin{pmatrix} 0, \mathbf \Sigma \end{pmatrix} \\
\mathbf \Sigma & = \mathbf{S} \mathbf{R} \mathbf{S}, \ \mathbf{S} = \begin{bmatrix} \sigma_u & 0 \\ 0 & \sigma_\gamma \end{bmatrix}, \ \mathbf R = \begin{bmatrix} 1 & \rho \\ \rho & 1 \end{bmatrix} \\
\sigma_u, \sigma_\gamma & \sim \text{Exponential}(1)\\
\mathbf R     & \sim \operatorname{LKJcorr}(1)
\end{aligned}$$
</div>

```{r poissonExampleVaryingTime, echo=FALSE, message=FALSE, results='hide', warning=FALSE, fig.height=3.5, fig.width=4}
library(data.table)
library(brms)
library(ggplot2)

brmVaryingTime = brm(y ~ 0 + Intercept + (1 + iS|iN),
                     data = timeData,
                     family = poisson(link = "log"),
                     prior = c(prior(normal(3, 1), class = "b", coef = "Intercept"),
                               prior(exponential(1), class = "sd"),
                               prior(lkj_corr_cholesky(1), class = "L")),
                     cores = 2,
                     chains = 2,
                     iter = 4000,
                     file = "mdl/varyingTime", 
                     file_refit = "on_change")

fittedVaryingTime = fitted(brmVaryingTime)[, 1]

gpVaryingTime = 
  ggplot(data = timeData,
         aes(x = iS,
             y = log(y),
             group = iN)) +
  # geom_point(size = 5,
  #            alpha = 0.25,
  #            shape = 21,
  #            stroke = FALSE,
  #            fill = "black") +
  geom_line(mapping = aes(y = log(fittedVaryingTime),
                          colour = "Varying time"),
            size = 1,
            alpha = 0.3) +
  CreateBaseTheme() +
  labs(x = bquote(bold(x[i])),
       y = bquote(bold(hat(y[i])))) +
  scale_x_continuous(labels = fg,
                     breaks = c(0, 1, 2)) +
  scale_y_continuous(breaks = yBreaks,
                     labels = yLabels) +
  scale_colour_viridis_d(name = NULL, 
                         option = "C",
                         begin = 0.33,
                         end = 0.75) +
  theme(legend.position = "none")

gpVaryingTime
```
